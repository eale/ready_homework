import turtle
scr = turtle.Screen()
scr.setup(800, 600)
c = turtle.Turtle()
c.shape('circle')
c.color('#000080')

c.speed('fastest')
c.up()
s = turtle.Turtle()
s.shape('square')
s.color('#008000')

s.speed('fastest')
s.up()
c.goto(0,280)
c.stamp()
k = 0
for i in range(1,17):
    y = 30*i
    for j in range(i-k):
        x = 30*j
        s.goto(x,-y+280)
        s.stamp()
        s.goto(-x,-y+280)
        s.stamp()
    if i%4 == 0:
        x = 30*(j+1)
        c.color('#FF0000')
        c.goto(-x,-y+280)
        c.stamp()
        c.goto(x,-y+280)
        c.stamp()
        k+=2
    if i%4==3:
        x=30*(j+1)
        c.color('#FFD700')
        c.goto(-x,-y+280)
        c.stamp()
        c.goto(x,-y+280)
        c.stamp()
s.color('#8B4513')
for i in range(17,  20):
    y = 30 * i
    for j in range(3):
        x = 30 * j
        s.goto(x, -y+  280)
        s.stamp()
        s.goto(-x,-y+280)
        s.stamp()